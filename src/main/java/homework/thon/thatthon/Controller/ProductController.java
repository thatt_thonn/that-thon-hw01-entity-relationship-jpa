package homework.thon.thatthon.Controller;

import homework.thon.thatthon.Models.Product;
import homework.thon.thatthon.Models.PurchaseOrder;
import homework.thon.thatthon.Repository.ProductRepositoy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@ResponseBody
@RequestMapping("/purchase")
public class ProductController {

    private ProductRepositoy productRepositoy;

    @Autowired
    public void setPurchaseRepositoy(ProductRepositoy productRepositoy) {
        this.productRepositoy = productRepositoy;
    }

    @GetMapping
    private List<Product> allProduct(){
        return productRepositoy.allProduct() ;
    }

    @GetMapping("/save")
    private PurchaseOrder save(){
        Product product1=new Product("Coca",10,12.25);
        PurchaseOrder purchaseOrder=new PurchaseOrder("Customer 1","Male");
        product1.getPurchaseOrders().add(purchaseOrder);
        productRepositoy.save(product1);
        return purchaseOrder;
    }

    @GetMapping("/remove")
    private String remove(
            @RequestParam(defaultValue = "1",required = false, value = "id") Integer id
    ){
        productRepositoy.remove(id);
        return "true";
    }

    @GetMapping("/modify")
    private String modify(
            @RequestParam(defaultValue = "1",required = false,value = "id") Integer id
    ){
        productRepositoy.modify(id);
        return "true";
    }
}
