package homework.thon.thatthon.Models;


import javax.persistence.*;
import java.util.List;

@Entity
public class PurchaseOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "customer_name")
    private String custName;
    private String gender;

    @ManyToMany(mappedBy = "purchaseOrders",fetch = FetchType.LAZY)
    private List<Product> productList;

    public PurchaseOrder() {
    }

    public PurchaseOrder(String custName, String gender) {
        this.custName = custName;
        this.gender = gender;
    }

    public PurchaseOrder(String custName, String gender, List<Product> productList) {
        this.custName = custName;
        this.gender = gender;
        this.productList = productList;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getCustName() {
        return custName;
    }

    public void setCustName(String custName) {
        this.custName = custName;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public void setProductList(List<Product> productList) {
        this.productList = productList;
    }



}
