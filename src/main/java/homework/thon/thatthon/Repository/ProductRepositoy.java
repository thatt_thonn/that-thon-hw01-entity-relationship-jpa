package homework.thon.thatthon.Repository;

import homework.thon.thatthon.Models.Product;
import homework.thon.thatthon.Models.PurchaseOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ProductRepositoy {

    @PersistenceContext
    private EntityManager entityManager;

    public void save(Product product){
        entityManager.persist(product);
    }

    public List<Product> allProduct(){
        return entityManager.createQuery("select p from Product p").getResultList();
    }

    public void remove(Integer id){
        Product product=entityManager.find(Product.class,id);
        entityManager.remove(product);
        return;
    }

    public void modify(Integer id){
        Product p=entityManager.find(Product.class,id);
        p.setName("Sprite");
        p.setQty(15);
        entityManager.flush();
        return;

    }

}
